import time
import pickle
from sklearn.linear_model import LinearRegression
import numpy as np
import matplotlib.pyplot as plt


fichier = open("teste1.txt", "r")
X = []
Y = []
i = 0
ecg = []
ppg = []

for ligne in fichier:
    d = ligne.split(",")
    ecg.append(float(d[0]))
    ppg.append(float(d[1]))
    i += 1
    if (i>1000):
        X.append(ecg)
        Y.append(ppg)
        ecg = []
        ppg = []
        i = 0
    
fichier.close()
reg = LinearRegression().fit(X, Y)
t = reg.score(X, Y)
print(t)

fichier = open("teste3.txt", "r")
Xtest = []
Ytest = []
i = 0
ecg = []
ppg = []

for ligne in fichier:   
    d = ligne.split(",")
    #print(d[0])
    ecg.append(float(d[0]))
    ppg.append(float(d[1]))
    i += 1
    if (i>1000):
        Xtest.append(ecg)
        Ytest.append(ppg)
        ecg = []
        ppg = []
        i = 0 

for i in range(len(Ytest)):
    y = reg.predict([Ytest[i]])
    
    #print('------------------')
    t = Ytest[i]
    t1 = y[0]
#    for j in range(len(t1)):
    
#        print(t[j], t1[j])

data_train = open('model1.sav', 'wb')
pickle.dump(reg,data_train)
data_train.close()
        
plt.plot(Ytest[0])        
plt.show()
fichier.close()
