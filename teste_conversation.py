import time
import pickle
from sklearn.linear_model import LinearRegression
import numpy as np
import matplotlib.pyplot as plt


import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008

SPI_PORT   = 0
SPI_DEVICE = 0
mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

clf_file = open('model1.sav', 'rb')
clf = pickle.load(clf_file)

while True:
    t = reg.predict([mcp.read_adc(1)])
    print('Predict heart beat for Sensor1', t)
    time.sleep(0.2)
